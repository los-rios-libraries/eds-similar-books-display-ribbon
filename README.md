# README #

## NO LONGER UPDATED ##
This has been integrated into the EDS-Complete repository. Any further changes will be made there.

* * *

In EDS, catalog results have a couple detailed widgets that show up pointing users to "similar books" and "other books by this author." Since we include the Harvard Library Bibliographic Dataset in our EDS databases, in many cases these show up in those widgets, as do items from GVRL that we don't own. (There's no way to limit the items in those widgets to your library collection.)

This script, inserted into a detailed record widget, identifies items from the local catalog and adds a ribbon to them, identifying as being "in the library." It also applies a class to items from other databases, in case you want to hide them. (The problem with hiding all of those is that sometimes the widget will contain only those, so a user might open the widget only to find it empty.)

![screenshot](https://bitbucket.org/repo/ejLdya/images/2137516158-2015-03-23_21-12-35.png)
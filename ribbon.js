function bookDisplayCtrl(widgetType)
{
  var relatedBooks = window.parent.document.getElementById(widgetType);
  if (relatedBooks)
  {
    var books = window.parent.document.querySelectorAll('#' + widgetType + ' + div .item');
    for (var i = 0; i < books.length; i++)
    {
      var badDBs = /Harvard Library|HathiTrust/; // identifying these in case we want to style them somehow
      var LOIS = 'LOIS: Los Rios Online'; // local catalog books
      if (books[i].innerHTML.search(badDBs) > -1)
      {
        books[i].className += ' not-in-LOIS'; // apply class to those
      }
      else if (books[i].innerHTML.indexOf(LOIS) > -1)
      {
        var libRibbon = window.parent.document.createElement('div');
        libRibbon.setAttribute('class', 'lib-ribbon');
        libRibbon.innerHTML = 'In the Library';
        if (books[i].innerHTML.indexOf('In the Library') == -1) // need to do this so that it doesn't add another each time we click the widget heading
        { // get that ribbon in there
          books[i].insertBefore(libRibbon, books[i].childNodes[0]);
          books[i].className += ' in-LOIS'; // apply a class for styling
        }
      }
    }
  }
}
(function() {
similarString = 'related_information_widget_similar_books';
otherByString = 'related_information_widget_other_books_by_this_author';
var relatedInfo = window.parent.document.querySelector('#' + similarString + '');
var otherBooks = window.parent.document.querySelector('#' + otherByString + '');
// add event listeners. I don't understand why this works given that the elements take a while to appear, but it does...
if (relatedInfo)
{
  window.parent.document.querySelector('#' + similarString + ' + div h2').addEventListener('click', function ()
  {
    bookDisplayCtrl(similarString);
  });
}
if (otherBooks)
{
  window.parent.document.querySelector('#' + otherByString + ' + div h2').addEventListener('click', function ()
  {
    bookDisplayCtrl(otherByString);
  });
}
})();